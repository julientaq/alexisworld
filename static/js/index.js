
const content = document.querySelector("main");
document.firstElementChild.style.zoom = "reset";

let mainScale = 1;
var posX = 0;
var posY = 0;



// keep track of the position of the main element and zoom in place depending of the user position

function zoomPosition(c,e,id){
   
   

    const wH = window.innerHeight;
    const wW = window.innerWidth;   
    let mx = e.clientX;
    let my = e.clientY;
   let newScale = mainScale + e.deltaY * 0.005

    let cLeft = content.getBoundingClientRect().left ;
    let cTop = content.getBoundingClientRect().top ;
    let offsetLeft = cLeft > 0 ? Math.abs(cLeft - mx) : Math.abs(cLeft)  + Math.min(mx, content.getBoundingClientRect().width);
    let offsetTop = cTop > 0 ? Math.abs(cTop - my) : Math.abs(cTop) + Math.min(mx, content.getBoundingClientRect().height);
    
    console.log(newScale, mainScale);
   switch (id) {
    case "zoomout":
        return `${Math.abs(cLeft)+wW/2}px ${Math.abs(cTop)+wH/2}`;
        break;
        case "zoomout":
            return `${Math.abs(cLeft)+wW/2}px ${Math.abs(cTop)+wH/2}`;
            break;
    default:
    
        return `${offsetLeft/mainScale}px ${offsetTop/mainScale}px`;
        break;
   }


    
}

// return `${offsetLeft + offsetLeft*0.005}px ${offsetTop + offsetTop*0.005 }px`;

// zoom function updating the url to stay at scale on reload


function zoom(id,ev){
   
    let url = new URL(window.location);
    const zoomPos = zoomPosition(content,ev,id);
    switch (id) {
        case "zoomout":

            if (mainScale > 0.2) {
                mainScale = parseFloat(mainScale)-0.1;
        
                
                content.style.transformOrigin = zoomPos;
                const url = new URL(window.location);
                //updateUrl();
        
            } 
                
            content.style.transform = `scale(${mainScale})`;
            
            break;
    
        case "zoomin":
            
            let currentScale = parseFloat(mainScale)+0.1;
            mainScale = currentScale.toFixed(2);
            content.style.transformOrigin = zoomPos;
            content.style.transform = `scale(${currentScale})`;
            //updateUrl();
            
            break;

        case "onload":
            const params = new URLSearchParams(window.location.search);
            const zoomValue = params.get("zoom");
        
            mainScale = zoomValue != null ? zoomValue : 1;
            content.style.transform = `scale(${parseFloat(zoomValue)})`;
            content.style.top = "0px";
            content.style.left = "0px";
        break;

        case "pinch":
                
                mainScale -= ev.deltaY*0.005;
                mainScale = Math.max(mainScale,0.5);
                content.style.transformOrigin = zoomPos;
                content.style.transform = `scale(${parseFloat(mainScale)})`;
            
         
            
        break;
    }

}
// move function used on trackpad and touchscreens
function move(a,b){
        //console.log(posX,posY);
        content.dataset.x = content.style.left.slice(0,-2);
        content.dataset.y = content.style.top.slice(0,-2);
        
        
        content.style.top = `${posY}px`;
        content.style.left = `${posX}px`;


}

//Update Url to keep info on reload 

function updateUrl(){
    var url = new URL(window.location);

    url.searchParams.set('zoom', mainScale);      
    window.history.replaceState({},'',url);
}


// Events Listeners

if (document.querySelector("#zoomout")) {
    document.querySelector("#zoomout").addEventListener("click", (ev) => {
        
        zoom("zoomout",ev);
    }) 
}
if (document.querySelector("#zoomin")) {
    document.querySelector("#zoomin").addEventListener("click", (ev)=>{ zoom("zoomin",ev)}) 
}

  
document.querySelector('#showGrid').addEventListener('click', function () {
    
    document.querySelectorAll('.doublepage').forEach(page =>{page.classList.toggle("showgrid")});

})

window.addEventListener('wheel', (e) => {
 
   
    if (e.ctrlKey) {
        e.preventDefault();
        zoom("pinch",e);
    } 
    else {
        posX -= e.deltaX * 2;
        posY -= e.deltaY * 2;
        move(content.dataset.x, content.dataset.y,e);
    }

  }, { passive: false } );



setTimeout(function () {
    document.querySelectorAll('.content').forEach(element => {
        if(isOverflowed(element)) {
            element.classList.add('overflow')
        };
    })
}, 500)

function isOverflowed(element) {
    return element.scrollHeight > element.clientHeight || element.scrollWidth > element.clientWidth;
}


// resize observer to pause video
window.onload = () => {
    
    let videos = document.querySelectorAll('video');
   
    let isPaused = true; /* Flag for auto-paused video */
    let observer = new IntersectionObserver((entries) => {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                //console.log(entry);
                setTimeout(()=>{entry.target.play(); isPaused = false},2000);
            }
            else {entry.target.pause(); isPaused = true}
        });
    }, {threshold: 0.8});
/*Avec un threshold de 1 il faut que toute la video soit visible, avec seulement 1px en overflow
 et la video n'est pas suffisament visible. D'ou un threshold de 0.8 par exemple */
    for (let video of videos) {

        observer.observe(video);
    }
    var main =document.querySelector('.wrapper');
  

    // zoom("onload");


    
}


let movingEl = document.querySelector(".spanning");
let pageWrapper = document.querySelector(".wrapper")

document.addEventListener('keyup', event => {
  if (event.code === 'Space') {
    console.log('Space up', content.dataset.x,  content.dataset.y);
    posX = content.dataset.x;
    posY = content.dataset.y; 
    movingEl.style.display = "none";

  }
})

document.addEventListener('keydown', event => {
  if (event.code === 'Space') {
    // console.log('Space left');
    event.preventDefault()
    movingEl.style.display = "block";
    // movingEl.addEventListener('click', function() {
      // moveEl(document.querySelector('.wrapper'))
    // })
  }
})

document.querySelectorAll(".toc a").forEach(a => {
  a.addEventListener('click', function () {
    let element = document.querySelector(".wrapper")
    //element.style.top = 0;
   // element.style.left = 0;
  })
})


import interact from 
'https://cdn.interactjs.io/v1.10.11/interactjs/index.js'


interact('.spanning')
  .draggable({
    // enable inertial throwing
    inertia: true,
 
    listeners: {
      // call this function on every dragmove event
      move: dragMoveListener
  }
});

function dragMoveListener (event) {
  var target = pageWrapper;
 
  
  // keep the dragged position in the data-x/data-y attributes
  var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
  var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
  // target.classList.add("revert");
  
  // translate the element
//    
  //console.log(pX, event.dx);
  target.style.left =  x + 'px ';
  target.style.top = y + 'px';
  
  console.log(x,y);

  // update the posiion attributes
  target.setAttribute('data-x', x);
  target.setAttribute('data-y', y);
}


function dragEndListener (event) {
  var target = pageWrapper;
  target.classList.remove("revert");
}


// this function is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener