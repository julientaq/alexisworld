let markdownIt = require("markdown-it");
var markdownItAttrs = require('markdown-it-attrs');

module.exports = function (eleventyConfig) {


  // / ****************Markdown Plugins******************** /
  let options = {
    html: true,
    breaks: true,
    linkify: true
  };
  let markdownLib = markdownIt(options).use(markdownItAttrs);
  eleventyConfig.setLibrary("md", markdownLib);

  // / ****************END Markdown Plugins******************** /

  // custom class handling
// takes the last argument and generate custom styles. 
// the syntax is : "property_value"
 function customStyle(cS){
 
    var c = cS !== undefined ? cS.split(' ') : "";
    var d;
     if (typeof c === 'object') { 
       d = c.map (e => {
        
        if (e !== undefined) { return e.split('_').join(':')}
     });
    return d = d.join(';');
    } 
  }




  eleventyConfig.addPassthroughCopy({ "static/css": "/css" });
  eleventyConfig.addPassthroughCopy({ "static/fonts": "/fonts" });
  eleventyConfig.addPassthroughCopy({ "static/js": "/js" });
  eleventyConfig.addPassthroughCopy({ "static/images": "/images" });
  eleventyConfig.addPassthroughCopy({ "static/videos": "/videos" });



  eleventyConfig.addPairedShortcode("video", function (content, videoID, filename, Xstart, Xend, Ystart, Yend, zIndex, cStyle) {
  var d = customStyle(cStyle);
    return `<video id="${videoID}" style="z-index: ${zIndex}; grid-column: ${Xstart}/${Xend}; grid-row: ${Ystart}/${Yend}; ${d != undefined ? d.toString():""}"  muted autoplay loop  predload="none"> ${markdownLib.render(content)}
<source src="/videos/${filename}"  type="video/mp4">
Sorry, your browser doesn't support embedded videos.
</video>
`  });


  eleventyConfig.addPairedShortcode("image", function (content, imageID, filename, Xstart, Xend, Ystart, Yend, zIndex, alttext) {
    return `<figure id="${imageID}" style="z-index: ${zIndex}; grid-column: ${Xstart}/${Xend}; grid-row: ${Ystart}/${Yend}">
<img src="/images/${filename}" alt="${alttext ? alttext : ''}"/>
<figcaption>${markdownLib.render(content)}</figcaption>
</figure>`
});



eleventyConfig.addPairedShortcode("text", function (content, textID, textClass, Xstart, Xend, Ystart, Yend, zIndex , cS ){


  var d = customStyle(cS);
 

  
  return `<section id="${textID}" class="${textClass}" style="z-index: ${zIndex}; grid-column: ${Xstart}/${Xend}; grid-row: ${Ystart}/${Yend}; ${d != undefined ? d.toString():""}">${markdownLib.render(content)}</section>`
});

eleventyConfig.addPairedShortcode("title", function (content, titleID, Xstart, Xend, Ystart, Yend, zIndex, cS) {
  var d = customStyle(cS);
  return `<section id="${titleID}" style="z-index: ${zIndex}; grid-column: ${Xstart}/${Xend}; grid-row: ${Ystart}/${Yend}; ${d != undefined ? d.toString():""}"> ${markdownLib.render(content)} </section>`
});

  // folder structures
  // -----------------------------------------------------------------------------
  // content, data and layouts comes from the src folders
  // output goes to public (for gitlab ci/cd)
  // -----------------------------------------------------------------------------
  return {
    dir: {
      input: "src",
      output: "public",
      includes: "_layouts",
      data: "_data",
    },
  };
};


function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}


