---
title: La double page 4
tags: doublepage
version: 1
id: 17
permalink: false
gap: 0px
gapvertical: 13px
colonnecount: 9
linecount: 9
pagepadding: 13px
posx: 1
posy: 9
---

<figure style=" grid-column: 1/27; grid-row:1/10; height:842px ;overflow:visible; display:flex; flex-direction:column; position :absolute; top:-13px; left:-13px;z-index:1">
<img src="/images/17-18.jpg" style="align-self:left;width:auto; height:842px;z-index:10;opacity:1;">
</figure>

<section class="size5 ptableau" style="grid-column:7/27; grid-row:1; grid-template-rows:repeat(3,1fr); position:relative; top:1px; height:45px; z-index:3">
    <div class="pligne" style="grid-column:1/21">
        <span>Movie</span>
        <span style="grid-column:5/8;padding-right:18px;">Hooper</span>
    </div>
    <div class="pligne" style="grid-column:1/21">
        <span>Date</span>
        <span style="grid-column:5/21">1978</span>
    </div>
    <div class="pligne" style="grid-column:1/21">
        <span>Directed By</span>
        <ul style="grid-column:5/21">
            <li class="grey">Hal Needham</li>
            <li>John Landis (prologue & segment 1)</li>
            <li class="grey">George Miller</li>
            <li class="grey">Steven Spielberg</li>
        </ul>
    </div>

</section>

{%- title "title-02",, 7, 12, 1,2, 12 -%}

<h1 class="size6" style="position:relative; top:65px">Reid Rondell</h1>

{% endtitle %}

{%- text "", "description size6", 13, 27, 2, 6, 2,"padding-right_13px top_-28px position_relative" -%}

Reid Rondell était le benjamin de l’une des plus importantes lignées de cascadeurs. Son père Ronnie Rondell Jr. est un cascadeur légendaire de Hollywood et le cofondateur de l’organisation de cascadeurs et cascadeuses Stunts Unlimited (avec Hal Needham et Glenn Wilder) en 1970. C’est lui que l’on peut voir en flammes au milieu des soundstages de la Warner sur la pochette de l’album <span style="font-style:italic">Wish You Were Here</span> de Pink Floyd. Le frère de Reid Rondell, R.A. Rondell, est également un cascadeur, coordinateur et réalisateur de seconde équipe très connu. Son fils Erik Rondell est lui-même un cascadeur à la longue carrière. Il joue aussi le rôle de Johnny Horne dans deux épisodes de la série <span style="font-style:italic">Twin Peaks</span> (Lynch, 2017), en particulier celui où, muni d’un casque et attaché à une chaise, il se débat au sol impuissant, un appareil placé dans sa bouche l’empêchant même de parler.

{% endtext %}

<section class="size5 ptableau" style="grid-column:7/27; grid-row:3; grid-template-rows:repeat(3,1fr); position:relative; top:7px; height:35px; z-index:3">
    <div class="pligne" style="grid-column:1/21">
        <span style="grid-column:1/4">Production Company</span>
        <span style="grid-column:5/8;padding-right:18px;">Warner Bros.</span>
    </div>
    <div class="pligne" style="grid-column:1/21">
        <span>Distrobutor</span>
        <span style="grid-column:5/21">Warner Bros.</span>
    </div>
</section>

<figure style=" grid-column: 7/12; grid-row:3; position :relative; z-index:15; width:100%; top: 38px">

<img src="/images/wishyouwerrehere17.jpg" style="height:auto; width:100%;position:relative;z-index:10;opacity:1; grid-row:1">

<figcaption class="size5"style="display:flex; flex-direction:row; grid-row:2; padding:4px 0 1px 0 ; width:400px; letter-spacing: -0.15px; top: -5px; position: relative;">

<p style="position:relative; left:-129px; ">1</p>
<p style="position:relative; left:-5px; ">Shooting pour la pochette de <span style="font-style: italic;">Wish You Were Here</span> (Pink Floyd, 1975) aux studios de la Warner Bros. en 1975. Image trouvée sur le site <a href="https://colecciones.tumblr.com/post/156941945110/stuntmen-ronnie-rondell-warner-bros-backlot-in">https://colecciones.tumblr.com/post/156941945110/stuntmen-ronnie-rondell-warner-bros-backlot-in </a></p>
</figcaption>
</figure>

{%- text "", "description size15 greytowhite", 7, 27, 5, 6, 2 ,"position_relative top_-42px letter-spacing_-1px"-%}
C’est lui que l’on peut voir en flammes au <br>milieu des soundstages de la Warner sur <br>la pochette de l’album Wish You Were Here <br>de Pink Floyd.
{% endtext %}

<figure style=" grid-column: 7/12; grid-row:8; height:auto ;overflow:visible; position :relative ;z-index:1; top:8px">
<img src="/images/needhamrondell.jpg" style="width:auto; height:auto; width:100%; z-index:10;opacity:1;">
</figure>

<figure style=" grid-column: 7/12; grid-row:9; position :relative; z-index:15; width:100%; bottom: 13px">

<img src="/images/needhamronniereid.jpg" style="height:auto; width:100%; position:relative;z-index:10;opacity:1; grid-row:1">

<figcaption class="size5"style="display:flex; flex-direction:row; grid-row:2; padding:4px 0 1px 0 ; width:400px; letter-spacing: -0.15px; top: -5px; position: relative;">

<p style="position:relative; left:-129px; ">2</p>
<p style="position:relative; left:-5px; ">Les cascadeurs et co-fondateurs de Stunt Unlimited Hal Needham et Ronnie Rondell après une cascade réussie de ce dernier. Reid est à droite. Image tirée de l’épisode de la série télévisée documentaire Stuntmasters diffusé le 10 novembre 1991, filmé sur un écran de télévision et mis en ligne le 3 décembre 2013 sur la chaîne YouTube de 240RobertA (<a href="https://www.youtube.com/watch?v=3LFdknLbcwY&t=290s">https://www.youtube.com/watch?v=3LFdknLbcwY&t=290s</a>).</p>
</figcaption>
</figure>