---
title: La double page 4
tags: doublepage
version: 1
id: 3
permalink: false
gap: 0px
gapvertical: 13px
colonnecount: 9
linecount: 9
pagepadding: 13px
posx: 1
posy: 2
---


<figure style=" width:190px; ;left:380px ; top:-177px; overflow:hidden; position: absolute">
<img src="/images/solarisation4.jpg" style="position:relative;width:100%; height:auto;z-index:10;opacity:0.25; top:0px;transform:rotate(90deg)"></figure>





{%- text "", "description size15 ligthgrey", 6, 27, 9, 10, 2 ,""-%}

Il en est de même pour l’actrice Taylor Hickson, gravement blessée au visage par du verre 
sur le tournage du film d’horreur (Incident in a) Ghostland (Laugier, 2018). 

{% endtext %}

<figure style=" position:fixed; height:600px; width:250px ;top:130px;left:-100px; background-color:white; position:absolute; overflow:hidden; z-index:1"><img src="/images/solarisation1-1.jpg" style=" position:absolute; width: 400px; right:0px; opacity:0.25" ></figure>

<section class="size5 ptableau" style="grid-column:7/26; grid-row:1; grid-template-rows:1fr 1fr 1fr 1fr; position:relative; top:0px; height:30px;">
 <div class="pligne" style="grid-column:1/21">
        <span  style="grid-column:1/5">Distributor</span>
        <span style="grid-column:5/12">Screen Gems</span>
    </div>
</section>

<figure style=" grid-column: 7/11; grid-row:1; height:100px; overflow:hidden; display:flex; flex-direction:column; position :relative; top:14px">

<img src="/images/AG-LRE_PF_REFC_MJ_I02.jpg" style=" width:100%;z-index:10;opacity:1 ;">

</figure>

<figure style=" grid-column: 7/11; grid-row:2; height:100px; overflow:hidden; display:flex; flex-direction:column; position :relative; top:14px">

<img src="/images/AG-LRE_PF_REFC_MJ_I01.jpg" style=" width:100%;z-index:10;opacity:1 ;">

</figure>

<figure style=" grid-column: 7/11; grid-row:3; height:100px; overflow:hidden; display:flex; flex-direction:column; position :relative; top:15px">

<img src="/images/AG-LRE_PF_REFC_MJ_I03.jpg" style="width:100%;z-index:10;opacity:1 ;">

</figure>





{%- text "", "size5", 7, 26, 4, 4, 2, "top_15px position_relative height_30px padding-right_13px" -%}
Images tirées du compte Instagram de l’actrice Milla Jovovich (https://www.instagram.com/millajovovich/). Publiées le 31 août, le 16 octobre et le  30 novembre 2015.
{% endtext %}